import Sequelize, { Model, ModelCtor } from 'sequelize';

import { UserProps } from '../interfaces/User';

import { sequelize } from './index';

const User: ModelCtor<Model<UserProps>> = sequelize.define('users', {
// const User: ModelCtor<Model<UserProps>> = sequelize.define('users', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
  },

  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },

  email: {
    type: Sequelize.STRING(126).BINARY,
    allowNull: false,
    unique: true,
  },

  password: {
    type: Sequelize.STRING,
    allowNull: false,
  },

  role: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});

export default User;
