import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';

import authRoutes from './routes/auth';
import usersRoutes from './routes/users';

dotenv.config({
  path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env',
});

const app = express();

app.use(cors());
app.use(express.json());

// swagger init
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./docs/swagger.yaml');

// routes
app.use('/api/auth', authRoutes);
app.use('/api/users', usersRoutes);

// Serve Swagger api
app.use('/api/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument, {
  explorer: false
}));

export default app;
