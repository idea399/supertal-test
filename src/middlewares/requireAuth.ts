import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import verifyToken from '../utils/verifyToken';
import User from '../models/User';

export default function requireAuth(acceptableRoles: string[] = []) {
  return async (req: Request, res: Response, next: NextFunction) => {
    let token: string = (req.headers['Authorization'] || req.headers['authorization']) as string;

    if (!token) {
      return res.status(401).json({
        success: false,
        message: 'Authorization failed. Please sign in again.'
      });
    }
  
    try {
      token = token.split("Bearer ")[1];

      let user: any = await verifyToken(token);
      user = await User.findByPk((user as any).id);

      if (!user) {
        return res.status(404).send('Invalid email or password.');
      };

      let hasPermission = false;
      if (acceptableRoles.length === 0 || acceptableRoles.indexOf(user.role.upperCase()) !== -1) {
        hasPermission = true;
      }

      // if user has no permission for current request
      if (!hasPermission) {
        return res.status(403).send('Authorization failed. You don\'t have permission.');
      }

      // set role flags
      user.isAdmin = false;
      user.isUser = false;

      if (user.role === 'USER') {
        user.isUser = true;
      } else {
        user.isAdmin = true;
      }

      req.user = user;
    } catch (err) {
      console.log(err);
      return res.status(401).json({
        success: false,
        message: 'Token has been expired. Please sign in again.'
      });
    }
  
    return next();
  };
}
