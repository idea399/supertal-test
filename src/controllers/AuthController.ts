import { Request, Response } from 'express';
import bcrypt from 'bcrypt';

import generateToken from '../utils/generateToken';

import User from '../models/User';

class AuthController {
  // sign in user
  static async signIn(req: Request, res: Response) {
    const { email, password } = req.body;

    try {
      const user = await User.findOne({ where: { email: email } }).then((r) =>
        r?.get()
      );

      if (user) {
        // check if password is correct
        const isMatch = bcrypt.compareSync(password, user.password as string);

        if (isMatch) {
          res.json({
            user,
            access_token: generateToken(user),
          });
        } else {
          res.status(400).json({
            message: 'Email or password is incorrect.',
          });
        }
      } else {
        res.status(400).json({
          message: 'Email or password is incorrect.',
        });
      }
    } catch (err) {
      res.status(500).json({
        message: 'Something went wrong',
      });
    }
  }
}

export default AuthController;
