import { Request, Response } from 'express';
import bcrypt from 'bcrypt';

import generateToken from '../utils/generateToken';

import User from '../models/User';

class UserController {
  // list all users
  static async listUsers(req: Request, res: Response) {
    // get user list
    const users = await User.findAll({
      attributes: ['id', 'name', 'email', 'role'],
    });

    return res.json(users);
  }

  // Get a user by id
  static async getUser(req: Request, res: Response) {
    const { id } = req.params;

    try {
      // searching in database if a user with this email send on red.body exists
      const user = await User.findOne({
        where: {
          id,
        },
      }).then((r) => r?.get());

      if (!user) {
        return res.status(400).json({ message: 'Not found a user.' });
      }

      return res.json({ user: user });
    } catch (err) {
      return res.status(500).json({ message: 'Something went wrong.' });
    }
  }

  // create a new user
  static async createUser(req: Request, res: Response) {
    const { name, email, password, role } = req.body;

    // password validation
    const passwordValidCheckReg =
      /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})/;
    if (!passwordValidCheckReg.test(password)) {
      return res.status(400).json({
        message:
          'Please use strong password. Password should be min 8 chars, contains uppercase, lowercase, number, and symbol.',
      });
    }

    // check if user already exists
    const user = await User.findOne({
      where: {
        email,
      },
    }).then((r) => r?.get());

    if (user) {
      return res.status(400).json({
        message: 'Email already exists. Please use another one.',
      });
    }

    const salt = bcrypt.genSaltSync(10);
    const passwordHash = bcrypt.hashSync(password, salt);

    // updating user data
    const newUser = await User.create({
      name,
      email,
      password: passwordHash,
      role,
    });

    // token
    const token = generateToken({ id: newUser.getDataValue('id') });

    return res.json({
      id: newUser.getDataValue('id'),
      access_token: token,
    });
  }

  // update a user
  static async updateUser(req: Request, res: Response) {
    const { name, email, role } = req.body;
    const { id } = req.params;

    try {
      // updating user data
      await User.update(
        {
          name,
          email,
          role,
        },
        {
          where: { id },
        }
      );

      return res.json({ id: id, message: 'User updated.' });
    } catch (err) {
      return res.status(500).json({ message: 'Something went wrong.' });
    }
  }

  // update password of user
  static async updatePassword(req: Request, res: Response) {
    const { id, password } = req.body;

    const salt = bcrypt.genSaltSync(10);
    const passwordHash = bcrypt.hashSync(password, salt);

    try {
      // updating user data
      await User.update(
        {
          password: passwordHash,
        },
        {
          where: { id },
        }
      );

      return res.json({ id: id, message: 'Password updated.' });
    } catch (err) {
      return res.status(500).json({ message: 'Something went wrong.' });
    }
  }

  // Delete a user by id
  static async deleteUser(req: Request, res: Response) {
    const { id } = req.params;

    try {
      await User.destroy({
        where: { id: id },
      });

      return res.json({ id: id, message: 'Deleted successfully.' });
    } catch (err) {
      return res.status(500).json({ message: 'Something went wrong.' });
    }
  }
}

export default UserController;
