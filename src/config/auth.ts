const authConfig = {
  secret: process.env.AUTH_SECRET || 'SUPERTAL_TEST_AUTH_CONFIG_STRING',
};

export default authConfig;
