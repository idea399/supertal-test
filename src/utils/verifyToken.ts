import jwt from 'jsonwebtoken';

import authConfig from '../config/auth';

// jwt sign token generator
const verifyToken = async (tokenString: string) => {
  const token = jwt.verify(tokenString, authConfig.secret);

  return token ? token : null;
};

export default verifyToken;
