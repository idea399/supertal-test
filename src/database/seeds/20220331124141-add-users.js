'use strict';
const bcrypt = require('bcrypt');

module.exports = {
  async up(queryInterface) {
    /**
     * Add seed commands here.
     */
    const admin = {
      id: 1,
      name: 'Test Admin',
      email: 'admin@primaku.com',
      password: '#Admin123',
      role: 'admin',
    };


    const salt = bcrypt.genSaltSync(10);
    await queryInterface.bulkInsert(
      'users',
      [
        {
          ...admin,
          password: bcrypt.hashSync(admin.password, salt),
        },
      ],
      {}
    );
  },

  async down(queryInterface) {
    /**
     * Add commands to revert seed here.
     */
    await queryInterface.bulkDelete('users', null, {});
  },
};
