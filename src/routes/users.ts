import express from 'express';

import UsersController from '../controllers/UserController';
import requireAuth from '../middlewares/requireAuth';

const routes = express.Router();

// post - authentication
routes.get('/', requireAuth(), UsersController.listUsers);
routes.get('/:id', requireAuth(), UsersController.getUser);
routes.post('/', requireAuth(), UsersController.createUser);
routes.patch('/:id', requireAuth(), UsersController.updateUser);
routes.put('/password', requireAuth(), UsersController.updatePassword);
routes.delete('/:id', requireAuth(['ADMIN']), UsersController.deleteUser);

export default routes;
