import express from 'express';

import Authcontroller from '../controllers/AuthController';

const routes = express.Router();

routes.post('/signin', Authcontroller.signIn);

export default routes;
