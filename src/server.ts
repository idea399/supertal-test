import app from './app';

import { sequelize } from './models/index';

const PORT = process.env.PORT || 8000;

sequelize
  .sync()
  .then(() => {
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
  })
  .catch((err) => {
    console.log(`Error on database, ${err}.`);
  });
