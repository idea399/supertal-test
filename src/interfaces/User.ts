// User model types
export interface UserProps {
  id?: number;
  name: string;
  email: string;
  password?: string;
  role: 'ADMIN' | 'USER';
}
