## Pre requirements

- Node, NPM, Yarn (Optional), MySQL should be installed

- Create and setup .env file by following .env.example

## Available Scripts
- yarn prestart
- yarn start
- yarn build
- yarn predev
- yarn dev
- yarn pretest
- yarn test

## Swagger Api Url

- http://localhost:8000/api/swagger