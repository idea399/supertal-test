import faker from 'faker';
import factory from 'factory-girl';
// models
import User from '../../src/models/User';
// interfaces
import { UserProps } from './../../src/interfaces/User';

factory.define<UserProps>('User', User, {
  name: faker.name.findName(),
  email: faker.internet.email(),
  password: '123456',
  role: 'ADMIN',
});

export default factory;