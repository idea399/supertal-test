import { sequelize } from '../../src/models/index';
import factory from '../utils/factories';
import bcrypt from 'bcrypt';

import { UserProps } from './../../src/interfaces/User';
import generateToken from './../../src/utils/generateToken';

export default async function () {
  await sequelize.truncate({ force: true, cascade: true });
  await factory.cleanUp();

  const salt = bcrypt.genSaltSync(10);

  const adminData = {
    id: 1000,
    email: 'admin@example.com',
    name: 'Test admin',
    password: '123456',
  };

  await factory.create<UserProps>('User', {
    ...adminData,
    password: bcrypt.hashSync(adminData.password, salt)
  }).then((r) => r.id);

  const token = 'Bearer ' + generateToken(adminData);

  return {
    ...adminData,
    token: token,
  };
}
