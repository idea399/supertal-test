import request from 'supertest';
import * as faker from 'faker';

import app from '../../src/app';

import truncate from '../utils/truncate';

describe('Session authentication', () => {
  let adminData: any;
  // truncating tables before each test
  beforeEach(async () => {
    adminData = await truncate();
  });

  it('should authenticate user with valid credentials', async () => {
    const response = await request(app).post('/api/auth/signin').send({
      email: adminData?.email,
      password: adminData?.password,
    });

    expect(response.body).toHaveProperty('access_token');
  });

  it('should return error with invalid email', async () => {
    const response = await request(app).post('/api/auth/signin').send({
      email: faker.internet.email(),
      password: '12345678',
    });

    expect(response.status).toBe(400);
  });

  it('should return error with invalid password', async () => {
    const response = await request(app).post('/api/auth/signin').send({
      email: adminData.email,
      password: '12345',
    });

    expect(response.status).toBe(400);
  });
});
