import request from 'supertest';
import * as faker from 'faker';

import app from '../../src/app';

import truncate from '../utils/truncate';

describe('User create', () => {
  // truncating tables before each test
  let adminData: any;
  beforeEach(async () => {
    adminData = await truncate();
  });

  it('should return token with valid user info', async () => {
    const response = await request(app)
      .post('/api/users')
      .set('Authorization', adminData?.token)
      .send({
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: 'ValidatedPassword111!@#',
        role: 'ADMIN',
      });

    expect(response.body).toHaveProperty('access_token');
  });

  it('should return error with weak password', async () => {
    const response = await request(app)
      .post('/api/users')
      .set('Authorization', adminData?.token)
      .send({
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: '1234',
        role: 'ADMIN',
      });

    expect(response.status).toBe(400);
  });
});

describe('Get User data', () => {
  // truncating tables before each test
  let adminData: any;
  beforeEach(async () => {
    adminData = await truncate();
  });

  it('should return user data', async () => {
    const response = await request(app)
      .get(`/api/users/${adminData?.id}`)
      .set('Authorization', adminData?.token);

    expect(response.status).toBe(200);
  });

  it("should return error if user doesn't exists", async () => {
    const response = await request(app)
      .get(`/api/users/10000000`)
      .set('Authorization', adminData?.token);

    expect(response.status).toBe(400);
  });
});

describe('User data update', () => {
  // truncating tables before each test
  let adminData: any;
  beforeEach(async () => {
    adminData = await truncate();
  });

  it('should update user data', async () => {
    const response = await request(app)
      .patch(`/api/users/${adminData?.id}`)
      .set('Authorization', adminData?.token)
      .send({
        name: 'new name',
        email: 'new@example.com',
        role: 'MENTO',
      });

    expect(response.status).toBe(200);
  });
});
