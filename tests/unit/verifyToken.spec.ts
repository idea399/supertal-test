import generateToken from '../../src/utils/generateToken';
import verifyToken from '../../src/utils/verifyToken';

describe('Authentication token generator', () => {
  it('should return token for valid params', async () => {
    const tokenString = generateToken({ id: 1 });
    
    const tokenObj = await verifyToken(tokenString as string);

    expect((tokenObj as any).id).toEqual(1);
  });
});
