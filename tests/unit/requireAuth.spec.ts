import request from 'supertest';

import app from '../../src/app';

import truncate from '../utils/truncate';

describe('requireAuth middleware', () => {
  let adminData: any;
  beforeEach(async () => {
    adminData = await truncate();
  });

  it('should return error for inactive user', async () => {
    const response = await request(app)
      .get(`/api/users`)
      .set('Authorization', 'Bearer fake_token');

    expect(response.status).toBe(401);
  });

  it('should be authorized with active user', async () => {
    const response = await request(app)
      .get(`/api/users`)
      .set('Authorization', adminData?.token);

    expect(response.status).toBe(200);
  });
});
